from django.shortcuts import get_object_or_404, render

from .models import Product

# Create your views here.


def product_detail(request, pk):
    obj = get_object_or_404(Product, pk=pk)
    context = {"object": obj}
    return render(request, "product.html", context=context)


def products(request):
    context = {"object_list": Product.objects.all()}
    return render(request, "product.html", context=context)
