from functools import cached_property

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.urls import reverse

from simpel_qrcodes.models import LinkedQRCode

# Create your models here.


class Product(models.Model):

    name = models.CharField(max_length=100)

    qrcodes = GenericRelation(
        LinkedQRCode,
        object_id_field='linked_object_id',
        content_type_field='linked_object_type'
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Product"

    def get_absolute_url(self):
        return reverse("product_detail", kwargs={"pk": self.pk})

    @cached_property
    def qrcode(self):
        return self.qrcodes.first()
