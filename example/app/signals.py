import logging

from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Product

logger = logging.getLogger("simpel")


def generate_qrcode(instance):
    if instance.qrcodes.first() is None:
        url = instance.get_absolute_url()
        instance.qrcodes.create(name="public_url", qr_data=url)


@receiver(post_save, sender=Product)
def after_save_product(sender, instance, created, **kwargs):
    generate_qrcode(instance)
