from django.urls import path

from . import views

urlpatterns = [
    path("product/", views.products, name='product_index'),
    path("product_detail/<int:pk>/", views.product_detail, name='product_detail'),
]
