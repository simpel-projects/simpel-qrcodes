from django.contrib import admin

from .models import Product

# from simpel_qrcodes.admin import LinkedQRCodeInline


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ["name"]
    # inlines = [LinkedQRCodeInline]
